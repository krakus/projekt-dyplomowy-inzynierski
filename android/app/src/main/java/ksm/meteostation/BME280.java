package ksm.meteostation;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class BME280 {
    @SerializedName("id")
    private String id;
    @SerializedName("time")
    private Date date;
    @SerializedName("pressure")
    private int pressure;
    @SerializedName("temperature")
    private int temperature;
    @SerializedName("humidity")
    private int humidity;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }
}
