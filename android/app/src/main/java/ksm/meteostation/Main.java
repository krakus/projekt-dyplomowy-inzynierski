package ksm.meteostation;

public interface Main {

    interface Presenter{
        void requestData();
    }

    interface MainView {
        void applyRequestedData(BME280 bme280);
    }

    interface GetNoticeInteractor{

        interface OnFinishedListener{
            void onFinished(BME280 bme280);
            void onFailure(Throwable t);
        }

        void getBME280(OnFinishedListener onFinishedListener);
    }
}
