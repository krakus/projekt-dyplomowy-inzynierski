package ksm.meteostation;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    private static Retrofit retrofit;

    public static Retrofit getRetrofitInstance(){
        if(retrofit == null){
            retrofit = new retrofit2.Retrofit.Builder().baseUrl(Constants.restApiUrl).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
