package ksm.meteostation;

public class MainActivityPresenter implements Main.Presenter, Main.GetNoticeInteractor.OnFinishedListener {

    private Main.MainView mainView;
    private Main.GetNoticeInteractor interactor;

    public MainActivityPresenter(Main.MainView mainView, Main.GetNoticeInteractor interactor){
        this.mainView = mainView;
        this.interactor = interactor;
    }

    @Override
    public void requestData() {

    }

    @Override
    public void onFinished(BME280 bme280) {

    }

    @Override
    public void onFailure(Throwable t) {

    }
}
