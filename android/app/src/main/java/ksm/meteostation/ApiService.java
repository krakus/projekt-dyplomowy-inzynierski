package ksm.meteostation;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("/bme_280")
    Call<BME280> getBme280Measurement();
    @GET("/is_raining")
    Call<IsRaining> getIsRaining();

}
