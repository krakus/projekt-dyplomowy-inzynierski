package ksm.meteostation;

import com.google.gson.annotations.SerializedName;

public class IsRaining {
    @SerializedName("is_raining")
    private boolean isRaining;

    public boolean isRaining() {
        return isRaining;
    }

    public void setRaining(boolean raining) {
        isRaining = raining;
    }
}
