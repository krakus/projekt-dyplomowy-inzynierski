from flask import Flask, jsonify, render_template
import psycopg2
import driver
import RPi.GPIO as GPIO


app = Flask(__name__)
bme_address = 0x77
DevelopingOnWindows = False

@app.route('/')
def index():
    return render_template("home.html")

@app.route('/save', methods=['GET'])
def save_measurement():
    temperature, pressure, humidity = driver.readBME280All(bme_address)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(38, GPIO.IN)
    if (GPIO.input(38) == 1):
        raining = "not raining"
    else:
        raining = "raining"

    conn = psycopg2.connect(host='localhost', user='pi', password='raspberry', dbname='weatherstation', port='5432')
    cur = conn.cursor()
    sql = "INSERT INTO measurements(time, pressure, temperature, humidity, raining) values(NOW(), %s, %s, %s, %s);"
    val = (pressure, temperature, humidity, raining)
    cur.execute(sql, val)
    conn.commit()
    conn.close()
    app.logger.info("value saved")
    return 'Measurements saved:' + str(pressure) + ' ' + str(temperature) + ' ' + str(humidity) + ' ' + raining

@app.route('/measurements')
def measurements():
    conn = psycopg2.connect(host='localhost', user='pi', password='raspberry', dbname='weatherstation', port='5432')
    app.logger.info(conn.closed)
    cur = conn.cursor()
    cur.execute('select * from measurements;')
    conn.commit()
    measures = cur.fetchall()
    conn.close()
    return render_template('measurements.html', results=measures)

@app.route('/bme_280', methods=['GET'])
def measure_bme():
    if DevelopingOnWindows:
        measurement = {
            'pressure': 1027,
            'temperature': 24,
            'humidity': "35%"
        }
    else:
        temperature,pressure,humidity = driver.readBME280All(bme_address)
        measurement = {
        'pressure': pressure,
        'temperature': temperature,
        'humidity': humidity
    }   
    bme = measurement
    return jsonify('BME280', measurement)

@app.route('/is_raining', methods=['GET'])   
def isRaining():
    if DevelopingOnWindows:
        is_raining = { 'raining': 1}
    else:
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(38, GPIO.IN)
        raining = GPIO.input(38)
        is_raining = {'raining': raining }

    raining = is_raining
    return jsonify('is_raining', is_raining)
    
@app.route('/air_condition', methods=['GET'])
def measure_ccs811():
    measurement = {
    'co2': "xxx",
    'tvoc': "yyy"}
    return jsonify('co2', measurement)


@app.route('/humidity', methods=['GET'])
def measure_humi():
    temperature, pressure, humidity = driver.readBME280All(bme_address)
    return jsonify('humidity', humidity)

@app.route('/pressure', methods=['GET'])
def measure_press():
    temperature, pressure, humidity = driver.readBME280All(bme_address)
    return jsonify('pressure', pressure)

@app.route('/temperature', methods=['GET'])
def measure_temp():
    temperature, pressure, humidity = driver.readBME280All(bme_address)
    return jsonify('temperature', temperature)



app.run('0.0.0.0', '8080')
